package com.task03_Collection;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList();
        list.add(5);
        list.add(3);
        String name = "Sophia";
        Object object = name;
        Integer value = (Integer) object;
        list.add(value);
    }
}
