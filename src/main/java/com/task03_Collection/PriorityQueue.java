package com.task03_Collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class PriorityQueue<T extends Droid> extends Ship<T> {
    public PriorityQueue(T... droids){
        super(droids);
        Collections.sort(this.droids);
    }

    public void put(T droid){
        super.put(droid);
        Collections.sort(droids);
    }
}
